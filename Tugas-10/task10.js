import React from "react";
import ReactDOM from 'react-dom';
import './task10.css';

class TableBuah extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state =
        {
            dataHargaBuah:
            [
                {nama: "Semangka", harga: 10000, berat: 1000},
                {nama: "Anggur", harga: 40000, berat: 500},
                {nama: "Strawberry", harga: 30000, berat: 400},
                {nama: "Jeruk", harga: 30000, berat: 1000},
                {nama: "Mangga", harga: 30000, berat: 500}
            ]
        }
    }

    renderTableHeader()
    {
        let header = Object.keys(this.state.dataHargaBuah[0])
        return header.map((key, index) =>
        {
            return <th key={index}>{key.toUpperCase()}</th>
        })
    }

    renderTableData() {
        return this.state.dataHargaBuah.map((dataHargaBuah, index) => {
           const { nama, harga, berat } = dataHargaBuah //destructuring
           return (
              <tr key={nama}>
                 <td>{nama}</td>
                 <td>{harga}</td>
                 <td>{berat}</td>
              </tr>
           )
        })
     }

     render() {
        return (
           <div>
              <h1 id='title'>Tabel Harga Buah</h1>
              <table id='buah' style={{margin: "0 auto"}}>
                 <tbody>
                    <tr>{this.renderTableHeader()}</tr>
                    {this.renderTableData()}
                 </tbody>
              </table>
           </div>
        )
     }
}

export default TableBuah;
// eslint-disable-next-line no-undef
ReactDOM.render(<TableBuah />, document.getElementById('root'));